# Thema

## Gitlab 

DevOps in Gitlab mit Kubernetes

 Machbarkeitsstudie zur Verbesserung des Sofware-Entwicklungs-Workflow durch Integration der vorhandenen GitLab-Instanz mit einem Kubernetes-Cluster

### 1. Inhaltsverzeichnis

[[_TOC_]]

Abkürzungen

- AI:       Administration Intelligence AG
- VM:       Virtuelle Maschine
- CI:       Continuous Integration
- DevOps:   Zusammensetzung aus "development" und "operations". Modernes Software-Entwicklungs-Konzept
- on-prem/on premise: Betreiben von Software auf der lokal verfügbaren Hardware. Abgrenzung gegenüber einem Betrieb in der Cloud.

### 2. Einführung

Diese Projektdokumentation beschreibt den Ablauf des IHK-Abschlussprojektes, welche im Rahmen eines Praktikums als Teil der schulischen Ausbildung zum Fachinformatiker Systemintegration durchgeführt wurde. Als Praktikumsbetrieb stand die Administration Intelligence AG (AI) in Würzburg bereit. Die AI ist als Software-Haus Teil der Thome-Gruppe, ansässig in Würzburg und unterhält Niederlassungen in Berlin, Essen und Wien. Im Standort Würzburg arbeiten ca. 50 Mitarbeiter.

#### 2.1. Projektumfeld

Kernkompetenzen der AI ist die Entwicklung von Software-Lösungen für die Beschaffung über Ausschreibungen von Bund, Ländern und Kommunen. Die Produkte die hierfür zur Verfügung gestellt werden sind die AI Manager-Serie, die AI Assistenten-Serie, AI Plattform-Serie sowie die AI-Cockpitserie. Durch dieses Portfolio wird der komplette Ausschreibungsprozess effizient und rechtssicher von Ausschreibenden bis hin zu den sich bewerbenden Firmen abgedeckt und unterstützt. <https://www.ai-ag.de/>
Die AI arbeitet intern zur Versionsverwaltung des Quellcodes mit Gitlab. In einer zentralen Gitlab Umgebung sind alle Entwickler ihren Projekten und Teams zugeordnet. Die Software-Produkte werden hauptsächlich mit Java geschrieben. Die Kompilierung des Codes geschieht über Nacht durch einen Jenkins Server. Von Jenkins gesteuert wird eine Pipeline durchlaufen, an deren Ende lauffähiger Code im .jar-Format steht. Test-Systeme der verschiedenen Softwareprodukte werden auf virtuellen Maschinen (VM) in einem Proxmox-Cluster realisiert. Zur Installation freigegebene Versionen werden auf einem Netzlaufwerk (shared storage) samt Dokumentation abgelegt. Die Anwendungen werden in unterschiedlichen Environments in verschiedenen Versionen installiert und durchlaufen die Qualitätssicherung. Zusätzlich durchlaufen vom Customizing angepasste spezielle Kunden-Versionen ebenfalls diesen Workflow. Für das Hosting dieser vielen VM's werden die Ressourcen des firmeninternen Dienstleisters genutzt. Des Weiteren betreiben die Entwickler Installationen auf ihren Arbeitsrechnern.

#### 2.2. Projektziel

Es soll mit diesem Projekt aufgezeigt werden, welche Vorteile die Integration eines Kubernetes-Cluster mit Gitlab für die Software-Entwicklung bringt. Die relativ neue und recht komplexe Technologie Kubernetes soll an einem Beispiel genutzt werden um mit den gesammelten Erfahrungen Bereiche zu finden und abzugrenzen, in denen diese zukünftig angewendet werden kann, sowie die Herausforderungen analysiert werden, die bei Nutzung neuer Technologien immer auftreten. Die von Gitlab zu Verfügung gestellte Cluster-Anbindung soll genutzt werden, um den Ein- beziehungsweise Umstieg für Entwickler so unproblematisch wie möglich zu gestalten. Die technische Umsetztung soll mit dem Fokus auf einfache Adaption durch die Entwickler gestaltet sein.

#### 2.3. Projektbegründung

Kubernetes ist im Open-Source-Bereich eines der größten und am schnellsten wachsende Projekte und wird, in den unterschiedlichsten Kontexten, von sehr vielen und vor allem auch sehr großen Firmen genutzt (<https://kubernetes.io/case-studies/>, <https://docs.google.com/presentation/d/1zrfVlE5r61ZNQrmXKx5gJmBcXnoa_WerHEnTxu5SMco/edit#slide=id.g309d150e2f_0_1589>). Es handelt sich hier um Cloud-Technologie, die zur Verwatung und Hosting von Containern eigesetzt wird. Der Cluster-Betrieb containerisierter Software ist ressourcensparender und kann automatisiert den Bedürfnissen angepasste werden.
Kubernetes basierte Software-Entwicklung
Dadurch wird der Entwicklungsworkflow agiler und der Entwickler überblickt bis hin zum Deployment alle Schritte der Software und kann zusätzlich diese an wechselnde Anforderungen unkompliziert anpassen

Entwickler in Testing und Deployment mit einbeziehen
kürzere Rleasephsen

Mit diesem Projekt werden moderne Software-Entwicklungs-Methoden, die gerne mit dem Begriff DevOps zusammengefasst werden, praktisch in einem Pilot angewandt. Die gewonnen Erfahrungen werden bewertet und können in die Weiterentwicklung der angewandten Workflows eingebracht werden, wodurch die Qualität der Produkte steigt und die Entwicklungs-Kultur der Teams langfristigt gefördert wird. 

Devops:     dev-team und ops team näher zusammenbringen (Security, testing, management, Verantwortungsgefühl)
            schnell und agil durch direktes feedback
            automatisierung
            ständiges lernen, entwickeln im Team
            CI/CD


Cluster nutzt Ressourcen effektiver
CI CD
Devops
Container -> Kontrollierte/designte Umgebung
Scalability, Flexibility
Entwickler näher an die Pipeline bringen
Vorteile Cloud
Dependencies
Entwicklungszyklen verkürzen

#### 2.4. Projektabgrenzung

Aus Gründen der Komplexitätsreduktion wurde der "one-node"-Aufbau gewählt. Also eine Master-Einheit zur Steuerung und eine Worker-Einheit bilden das Cluster. Der Kubernetes-Cluster wird als Hosting-Plattform genutzt und es wird nicht auf automatisiertes Scaling eingegangen. Auch die "Rollback" und Ein Monitoring der Auslasten und statistische Auswertungen vor allem der Lastenverteilung ist nicht Teil des Projekts. Es wird auch nur die Implementation einer Anwendung mit dazugehöriger Datenbank betrachtet, welche einen rudimentären Testprozess durchläuft. Das Hauptaugenmerk liegt auf der Automatisierung des Build-Prozesses sowie dem unkomplizierten Zugriff durch den Entwickler auf die laufende Application und deren Update in der Gitlab-Umgebung.

### 3. Ist-Analyse


Die CI-Systeme werden nach Bedarf von technisch versierten Mitarbeitern aufgesetzt. Es werden Installations-Skripte eingesetzt, die ständig angepasst werden und 

keine Container, auch nicht gewollt aus supportgründen
Gitlab -> Jenkins -> CI -> VM
Entwickler installieren auf der Maschine
Diagramm

### 4. Anforderungen

Der Software-Entwicklungs-Workflow soll so gestaltet sein, dass die Entwickler in Gitlab ihren Code versionieren. Das Continuous Integration Feature von Gitlab wird genutzt den Java-Code mittels Maven zu kompilieren, in einer Registry ein Docker-Image abzulegen und die Application im Kubernetes-Cluster zu deployen. Abschließend wird mit ZaProxy <https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project> ein Scan der Application auf bekannte Sicherheitslücken und Schwachstellen durchgeführt und ein Bericht in Gitlab abgelegt. Die Pipeline, die die zuvor beschriebenen Schritte definiert, wird in Gitlab vorgehalten und ist für die Entwickler einsehbar. Hier können vom Entwickler Anpassungen vorgenommen werden oder Ergänzungen eingebracht werden. Während den einzelnen Schritten werden den Entwicklern Logs angezeigt, die in Gitlab für jeden Buildvorgang angezeigt und gespeichert werden. Ist die Build-Pipeline durchlaufen und läuft die Java-App im Cluster, wird diese über einen Link in einem neuen Browserfenster aufgerufen und kann einem ersten Review sowie Funktionstest unterzogen werden. Bei jeder Anpassung des Quellcodes kommt es automatisch zu einem erneuten Durchlauf der definierten Pipeline und zu einem Update der Anwendung.
Für die Entwickler ist dieser Prozess in Gitlab nachvollziehbar abgebildet und durch die Kontinuierliche Integration (continuous integration), nach einem definierten Schema, sind Features sofort einer ersten Beurteilung unterziehbar.
Aus Datenschutz Gesichtspunkten soll Code als geistiges Eigentum der AI betrachtet werden und nicht öffentlich zugänglich gemacht werden.
Anwendungen, die in Kubernetes laufen sollen, werden in sogenannten Pods betrieben. Ein Pod ist eine Zusammenstellung von Containern, die auf den Workern laufen und über das durch den Master verwaltete internen Netzwerk bei Bedarf miteinander kommunizieren können und von außerhalb des Clusters erreicht werden können. In Kubernetes werden Docker Container genutzt, weswegen die Apps in Containern lauffähig sein müssen.

Infrastructure as Code
Gitops
 
 Runner!!!

automatische deployment-pipeline
review
instant fixes
automate the boring stuff konzentration auf features


Kubernetes nutzen 
Für Entwickler intuitiv zu bedienen, Keine Kubernetes erfahrung nötig, bestehende/bewehrte Strukturen/Vorgänge nutzen, Im Browser Review, Artefakte/Images in Registry, NFS als ausgelagerter Storage, Datenschutz
CI/CD Devops, GitOps


### 5. Nutzwertanalyse

Zur Umsetzung des Projekts wird eine Kubernetes Cloud Infrastruktur benötigt. Die Kubernetes-Software selbst ist auf so gut wie allen Systemen lauffähig und es gibt die verschiedensten Möglichkeiten einen Cluster aufzusetzten. <https://kubernetes.io/docs/setup/pick-right-solution/> Gitlab kann mit bestehenden Kubernetes Instanzen verbunden werden oder es kann aus Gitlab heraus ein Kubernetes-Cluster auf Google mit Hilfe der Google Cloud Engine angelegt werden. Eine Recherche ergab, dass für das Projekt die drei großen Cloudprovider (Amazon, Google und Microsoft) als Dienstleister für den Betrieb von Kubernetes-Clustern in public clouds sowie der Betrieb in einer private Cloud auf der hausinternen Infrastruktur (on-prem) in Frage kommen.

Für die Betriebkosten der zwei VM's liegen keine offiziellen Zahlen vor. Eine Ansetzung in Höhe von 100 Euro pro Monat soll zum einen dafür sorgen, dass sehr geringe Kosten für den Betrieb "on premise" fälschlicherweise angenommen werden.  Es soll davon ausgegangen werden, dass der Betrieb einer Virtualisierungs-Umgebung in house vergleichbare Kosten mit von einem Diestleiste bereitgestellten Infrastruktur beinhaltet, da Administration und Verfügbarkeit erheblichen Aufwand erzeugt. 

cloud (AWS, Azure, GKE) gegen lokal, kubeadm gegen kubespray, minikube...
Ressourcenauslastung, Scalability, Container
make or buy
Datenschutz

Geschäftslogik

Kriterien: Preis, Installation kubernetes, Administration, Support, Location, Datenschutz, 

### 5.x Projektkosten

Die Projektkosten wurden auf einen Monat gerechnet, auch wenn das Projekt, das in dieser Arbeit begleitet wird, mit 

| Bezeichnung                | Berechnung          | Kosten in Euro |
| -------------              |:-------------:      | -----:         |
| Betriebskosten VM's        |                     | 100            |
| ZSA interner Dienstleister | 4h x 35 Euro        | 140            |
| Roland Benning (CI)        | 3h x 35 Euro        | 105            |
| Flo Kolb (Azubi)           | 5h x 25 Euro        | 125            |
| Alex Kotschenreuther       | 35 h x 7,50 Euro    | 262,50         |
| Gesamt                     |                     | 732,50         |

Bei angenommenen Entwicklungsstunden-Satz von 35 Euro:

732,50 Euro/ 35 h = 21 h

| Bezeichnung | Kosten in Euro  |--- |
|-------------|-------         :|--- |
| Betriebskosten VM's |150 |
|ZSA 4 x 35 |             140
Roland 3 x 35           105
Flo 5 x 25              125
alex 35 x 7,5           262,5
gesamt                  782,5

Die mit 7,50 Euro angesetzten Kosten für den Autor sind so niedrig, da es sich um ein unbezahltes Praktikum handelt
4h ZSA, 3h Muck, 3h Roland, 5h flo, Betrieb 2x VM 

Container sparen Ressourcen, keine Installation, Automatisierung, 


### 6. Konzept

Aus den Vorgaben, Analysen und Recherchen ergibt sich follgendes Konzept:
Ein Kubernetes-"Cluster" kann lokal in einem Docker-Container aufgesetzt werden <https://kubernetes.io/docs/setup/minikube/>. In dieser Variante sind Features wie Anbinden von NFS-Storage und einiges mehr nicht möglich, weswegen eine Umsetzung mit einer Master-Node und einer Worker-Node gewählt werden soll. 
Der Kubernetes Cluster setzt sich aus zwei VM's zusammen, die auf einem Proxmox-Cluster des Inhouse-Dienstleisters betrieben werden. Die Maschinen sind an firmeninterne, lokale Netzwerk angebunden und können per SSH administriert werden. Als Betriebssystem wird ein in der Proxmoxumgebung verfügbares Ubuntu 18.04 Image auf beide Maschienen aufgespielt. Die Hardwareanforderungen belaufen sich auf zwei CPU's, 8 GB RAM und 20 GB lokaler Speicher. Das ist über den Mindestanforderungen zum Aufsetzten des Cluster, da dieser auch noch für andere Projekte paralell oder im Anschluss genutzt werden soll. Ein NFS-Storage-Server ist über seine IP erreichbar und Anwendungen im Cluster können bei Bedarf den Speicher einbinden.
Da in der AI die Software-Produkte auf Java-Basis erstellt werden, wird eine Anwendung auf Spring-Boot-Basis gewählt. An diese Anwendung ist eine MySql Datenbank angebunden, die den durch den NFS-Server bereitgestellten Speicher nutzt.
Als Git Repository wird die lokale Gitlab Community Edition genutzt, die an den Kubernetes-Cluster angebunden wird. Um das AutoDevOps <https://docs.gitlab.com/ee/topics/autodevops/index.html#doc-nav> zu nutzen wird ein sogenannter "Runner" benötigt. Dieser wird an das Repository gebunden und ist dafür verantwortlich die in Gitlab definierte Build-Pipeline umzusetzen. Dabei werden, gesteuert durch den Runner, Docker Container im Cluster gestartet, die mit dem Quellcode aus Gitlab die definierten Jobs ausführen. Die Verwaltung und das Vorhalten von Dockercontainern werden durch eine lokale Docker-Registry realisiert. Diese ist ohne Authentifierung zu nutzen.
Zur Umsetzung wurde im Rahmen des Projektantrags ein Zeitplan erstellt, der im Anhang verlinkt ist.

Die angebundene Datenbank nutzt externen NFS-Storage, da die Datenbank im Cluster wie die App in einem Container läuft. So würden bei einem Neustart oder Umzug des Containers die gespeicherten Daten verloren gehen. Die von extern angebundene Speicher könnten auch andere MySql nutzen, was aber in diesem Projekt nicht umgesetzt wird

Eine Testing-Phase wird in die Pipeline integriert und mit einem ab

Einführung Kubernetes kurz, Devops
Diagramm
Dimensionierung VM's, Anbindung NFS
Spring-Boot + Mysql (NFS), Security: zaproxy
Runner in Cluster oder extern
zeitplan

### 7. Implementierung

Die beiden VM's, auf denen die Cluster-Installation 
Für das Setup des Kubernetes-Cluster <https://kubernetes.io/docs/setup/pick-right-solution/> wurde "kubeadm" genutzt und ein Cluster in Version v1.11.2 aufgesetzt. Das Tool "kubeadm" übernimmt die Installation und Grundkonfiguration aller für den Betrieb eines Kubernetes Cluster nötigen Software-Bestandteile. Die Entscheidung "kubeadm" zum Bootstrappen des Cluster zu verwenden fiel auf Grund der guten Dokumentation und der hohen Verbreitung. Zur clusterinternen Netzwerkverwaltung wird ein sogennates Container Network Interface (CNI) genutzt und die Wahl für den Test-Cluster fiel auf "flannel". Wie "kubeadm" ist auch "flannel" eines der am meisten verwendeten CNI's und zu Test-Zwecken absolut ausreichend.
Der Dokumentation <https://kubernetes.io/docs/setup/independent/install-kubeadm/> folgend läuft der Master auf der VM mit IP: 10.10.60.136 und Hostname "kub-master". Der Master stellt die Steuerzentrale des Clusters dar  Die VM mit IP: 10.10.60.200 wurde als Node also Worker eingebunden und hat "kub-node2-1" als Hostname. Vor der Installation von Kubernetes und der dafür nötigen Tools wurde auf beiden Maschinen der swap deaktiviert.
Zur Verwaltung und Installation von Anwendungen in Kubernetes-Clustern werden ähnlich wie in unixoiden Betriebsystemen unter anderem Paketmanager genutzt. Der momentan am weitest verbreitete ist "helm", der nach dem initialen Setup installiert wurde. "helm" arbeitet mit sogennaten "charts" die hier <https://github.com/helm/charts> eingesehen werden können und über die Commandozeile aufgerufen und installiert werden. Helm arbeitet mit "tiller", einem im Kubernetes-Cluster laufenden Diest, zusammen, dem die entsprechenden Berechtigungen zur Installation von Kompononten erteilt werden müssen. siehe Readme
Für die Nutzung des NFS-Storage-Servers werden die dazu benötigten Systemkomponenten auf der Worker-Node über 'sudo apt-get install nfs-common' installiert <https://wiki.ubuntuusers.de/NFS/>. Im Anschluss wird ein NFS-client im Cluster durch helm installiert <https://github.com/kubernetes-incubator/external-storage/tree/master/nfs-client>. Dem Installation-Aufruf werden die IP des Servers und das freigegebene Verzeichnis als Konfiguration mitgegeben (siehe Readme). Nach erfolgreicher Implementation des NFS-Clients können Container nun diesen Speicher über ein persisten volume claim (pvc) anfordern und Nutzen. Beim Deployment der Test-App wird auf das konkrete Vorgehen des persisten volume claim genauer eingegangen und in der Readme des Projekts können die Commands eingesehen werden.
Wie im Konzept dargelegt, ist der Gitlab-Runner als ausführende Instanz der zentrale Punkt der Gitlab-CI-Funktionen. Runner können auf jeglicher Infrastruktur mit Netzwerkanbindung an den Cluster betrieben werden. Hier wurde ein für das Projekt spezifische Runner über die vom Gitlab zu Verfügung gestellte helm-chart installiert und mit einem Token zur Authentifizierung an das Projekt gebunden. Nach der Installation und erfolgreicher Zuordung zum Projekt wird der Runner in den Settings in Gitlab angezeigt (Abbildung). to do secrets

### 8. Integration

autodevops kubernetes
.gitlab-ci.yml
Autodevops, cluster Token...
Anpassung docker Deamon 
```bash
{
          "insecure-registries" : ["docker.local:5000", "10.10.61.175:5000"]
}
```

lordgaav/dind-options:latest
admin.conf in container und auf docker hub, vorheriges projekt
Diagramm

### 8. Tests

Springboot-App, mysql
.gitlab-ci.yml
deployment.yml <https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/>
dns in deployment
```bash
...
dnsConfig:
  nameservers:
    - 10.10.60.250
  searches:
    - vm.local
```
zaproxy <https://docs.gitlab.com/ee/ci/examples/dast.html>

### 9.  Auswertung

Auswertung Artefakte ZaProxy
Vorraussetzung: Container
Devops-Gedanke, CI/CD
keine Domain, NodePort, Loadbalancer
soll-ist abgleich

### 10.  Übergabe

Betriebshandbuch/Readme/Wiki

Zu dem Projekt wurde ein Readme auf Gitlab erstellt 

### 11. Fazit

In diesem Projekt konnte gezeigt werden, wie die Kubernetes Integration von Gitlab genutzt werden kann, eine App aus dem Sourcecode automatisch in einen Cluster zu deployen. Die Rückmeldung des Maintainers des gewählten Projekts war sehr positiv. Die Automatisierung der Build-Pipeline sowie die schnelle und unkomplizierte 

Das Deployment über kubectl (siehe ...) sowie der in der deployment.yml verwendete Code sollte für Folgeprojekte verfeinert werden. Deployment eigener Apps über helm 

Für das Monitoring hält Gitlab auch Schnittstellen über ein Tool names "Prometheus" für einen Produktivbetrieb sehr wichtig. Die Implementierung von Metriken hätte den Rahmen des Projekts gesprengt

Die Wahl Gitlab und den Kubernete-Cluster lokal zu betreiben macht einige Anpassungen nötig. Gitlab auf gitlab.com mit Community Runnern und einer Anbindung an eine Kubernetes Instanz in der Googlecloud kann mit einigen wenigen Klicks aus Gitlab heraus aufgesetzt und voll integriert werden. 

In Zukunft auf Gitlab als alleiniges Tool zur CI und CD zu setzen sehe ich nicht als zielführend. Cluster-Technologie in den Software-Entwicklungs-Prozess einzubinden hat eine Menge Vorteile und sollte zukünftig mit einer Integration des Jenkins Servers und Kubernetes evaluiert werden.

Schöne Technik, steile Lernkurve, Administration
-> Zentrale Administration in House
Microservices
Container, on-prem?
cluster platzt 

Devops
Teams in den kompletten life-cycle des Produkts enger. Verantwortung und Knowledge, zeit und damit geld

Auch wenn hier nur eine on-premise Lösung getestet wurde, sehe ich in Zukunft Potential Cloud-Dieste einer der großen Diestleister mit einer private Cloud vor Ort zu kombinieren.
Infrastructure as Code

### 12. Anhang


In diesem Projekt wird in einer Testumgebung ein Kubernetes Cluster aufgesetzt und mit einem vorhandenen Klon der Git-Lab Produktiv-Umgebung angebunden. Es wird das in Git-Lab integrierte Autodevops-Feature genutzt eine Java Anwendung, da in der Firma vorwiegend mit Java gearbeitet wird,  aus Git-Lab heraus im Cluster zu implementieren.
In dieser Machbarkeitsstudie soll der Aufwand und der Nutzen abgeschätzt werden, Applikationen mit der Gitlab-CI direkt in einen Kubernetes-Cluster zu deployen.
Devops-Gedanke, CI/CD